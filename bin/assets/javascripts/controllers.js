'use strict';

/** Controllers */
angular.module('sseChat.controllers', ['sseChat.services'])
.directive('scrollBottom', function () {
  return {
    scope: {
      scrollBottom: "="
    },
    link: function (scope, element) {
      scope.$watchCollection('scrollBottom', function (newValue) {
        if (newValue)
        {
          $(element).scrollTop($(element)[0].scrollHeight);
        }
      });
    }
  }
})
.controller('ChatCtrl', function ($scope, $http, chatModel) {
    $scope.rooms = chatModel.getRooms();
    $scope.msgs = [];
    $scope.inputText = "";
    $scope.user = "Anonymous #" + Math.floor((Math.random() * 100) + 1);
    $scope.currentRoom = $scope.rooms[0];

    /** change current room, restart EventSource connection */
    $scope.setCurrentRoom = function (room) {
        $scope.currentRoom = room;
        $scope.chatFeed.close();
        
        $scope.listen();
    };

    /** posting chat text to server */
    $scope.submitMsg = function () {
        $http.post("/chat", { text: $scope.inputText, user: $scope.user,
            time: (new Date()).toUTCString(), room: $scope.currentRoom.value });
        $scope.inputText = "";
    };

    /** handle incoming messages: add to messages array */
    $scope.addMsg = function (msg) {
        $scope.$apply(function () { $scope.msgs.push(JSON.parse(msg.data)); });
    };

    /** start listening on messages from selected room */
    $scope.listen = function () {
        $scope.chatFeed = new EventSource("/chatFeed/" + $scope.currentRoom.value);
        $scope.chatFeed.addEventListener("message", $scope.addMsg, false);
    };

    $scope.listen();
});
